const path = require('path');

module.exports = {
    entry: ['./js/index.js'],
    output: {
        path: path.resolve(__dirname, './js'),
        //path: '/user/mac/Sites/komponenty/build',
        filename: 'bundle.js'
    },

    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }]
    }
}