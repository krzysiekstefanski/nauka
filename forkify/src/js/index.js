import axios from 'axios';

async function getResults(query) {
    const proxy = 'https://cors-anywhere.herokuapp.com/';
    const key = 'ec9d1b82582e69d13d93c1dc925c12ab';
    const res = await axios(`${proxy}http://food2fork.com/api/search?key=${key}&q=${query}`);
    console.log(res);
}
getResults('pizza');