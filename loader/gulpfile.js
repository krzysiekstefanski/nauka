var gulp = require ('gulp');
var merge = require('merge-stream');
var sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function (){
  var style = gulp.src('scss/style.scss', {style: 'compressed'}).pipe(sass({errLogToConsole: true})).pipe(sourcemaps.init()).pipe(cleanCSS()).pipe(sourcemaps.write()).pipe(gulp.dest('css'))
  var animacjaWejscia = gulp.src('scss/animacja-wejscia.scss', {style: 'compressed'}).pipe(sass({errLogToConsole: true})).pipe(sourcemaps.init()).pipe(cleanCSS()).pipe(sourcemaps.write()).pipe(gulp.dest('css'))
  return merge(style, animacjaWejscia);
});
gulp.task('watch', function(){
  gulp.watch('scss/**/*.scss', ['sass']);
});
