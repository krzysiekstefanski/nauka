setTimeout(function() {
 $(".nav").css('display', 'block')
 $(".space").css('display', 'block')
}, 7000);

console.log($('a[href="./inna.html"]'));
$('a[href="./inna.html"]').on('click', function(event) {
  event.preventDefault();
  $('.body-fill').animate({height: '100vh'}, "slow", function() {
    window.location.href = "./inna.html";
  });
})

function viewport(){
  var st = $(window).scrollTop();
  var wh = $(window).height();
  var top = st;
  var bottom = st + wh;

  var dif = $("nav").height() / $(document).height();

  $(".reference").css({
    'height': (wh * dif),
    'top': (st * dif)
  });
}

viewport();

$(window).resize(function(){
  viewport();
});

$(window).scroll(function(){
  viewport();
});

// $(document).on('click', 'a[href^="#"]', function (event) {
//     event.preventDefault();
//
//     $('html, body').animate({
//         scrollTop: $($.attr(this, 'href')).offset().top
//     }, 500);
// });

$(document).on('click', 'a[href="#yellow"]', function (event) {
    event.preventDefault();
    $('.b-blue').css('display', 'none')
    $('.b-green').css('display', 'none')
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500, function() {
      console.log("działa");
      $('.b-blue').css('display', 'block')
      $('.b-green').css('display', 'block')
    })
});
