var gulp = require ('gulp');
var merge = require('merge-stream');
var sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function (){
  var style = gulp.src('../assets/scss/register.scss', {style: 'compressed'}).pipe(sass({errLogToConsole: true})).pipe(sourcemaps.init()).pipe(cleanCSS()).pipe(sourcemaps.write()).pipe(gulp.dest('css'))
  // var portfolio = gulp.src('scss/portfolio.scss', {style: 'compressed'}).pipe(sass({errLogToConsole: true})).pipe(sourcemaps.init()).pipe(cleanCSS()).pipe(sourcemaps.write()).pipe(gulp.dest('css'))
  // var articles = gulp.src('scss/articles-all.scss', {style: 'compressed'}).pipe(sass({errLogToConsole: true})).pipe(sourcemaps.init()).pipe(cleanCSS()).pipe(sourcemaps.write()).pipe(gulp.dest('css'))
  return merge(style);
});
gulp.task('watch', function(){
  gulp.watch('../assets/scss/**/*.scss', ['sass']);
});
