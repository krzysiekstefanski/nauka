<?php
	include("includes/handlers/register-handler.php");
	include("includes/handlers/login-handler.php");
?>

<html>
<head>
	<title>Witaj w Spotify Clone!</title>
</head>
<body>

	<div id="inputContainer">
		<form id="loginForm" action="register.php" method="POST">
			<h2>Zaloguj się na swoje konto</h2>
			<p>
				<label for="loginUsername">Nazwa użytkownika</label>
				<input id="loginUsername" name="loginUsername" type="text" placeholder="bartSimpson" required>
			</p>
			<p>
				<label for="loginPassword">Hasło</label>
				<input id="loginPassword" name="loginPassword" type="password" placeholder="Twoje hasło" required>
			</p>

			<button type="submit" name="loginButton">ZALOGUJ SIĘ</button>

		</form>

		<form id="registerForm" action="register.php" method="POST">
			<h2>Zarejestruj swoje nowe konto</h2>
			<p>
				<label for="username">Nazwa użytkownika</label>
				<input id="username" name="username" type="text" placeholder="bartSimpson" required>
			</p>
			<p>
				<label for="firstname">Imię</label>
				<input id="firstname" name="firstname" type="text" placeholder="Bart" required>
			</p>
			<p>
				<label for="lastname">Nazwisko</label>
				<input id="lastname" name="lastname" type="text" placeholder="Simpson" required>
			</p>
			<p>
				<label for="email">Adres email</label>
				<input id="email" name="email" type="email" placeholder="bart@mail.pl" required>
			</p>
			<p>
				<label for="email2">Potwierdź adres email</label>
				<input id="email2" name="email2" type="email" placeholder="bart@mail.pl" required>
			</p>
			<p>
				<label for="password">Hasło</label>
				<input id="password" name="password" type="password" placeholder="Twoje hasło" required>
			</p>
			<p>
				<label for="password2">Potwierdź hasło</label>
				<input id="password2" name="password2" type="password" placeholder="Twoje hasło" required>
			</p>

			<button type="submit" name="registerButton">REJESTRACJA</button>

		</form>
	</div>

</body>
</html>
